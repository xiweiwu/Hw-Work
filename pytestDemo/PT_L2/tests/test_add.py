"""
用于定义测试加法的测试类
"""
import os.path
import allure
import pytest
from PT_L2.Base.base import Base
from PT_L2.utils.utils import Utils
from PT_L2.utils.log_util import logger

def get_add_datas(level='P0'):
    # 当前文件路径
    root_path = os.path.dirname(os.path.abspath(__file__))
    # 针对当前目录拼接yaml文件目录
    yaml_path = os.sep.join([root_path, '..', 'datas\clsAddData.yaml'])
    # 调用读取yaml文件方法
    yaml_datas = Utils.get_yaml_data(yaml_path)
    print(yaml_datas)
    datas = []
    ids = []
    # 将yaml数据转化为自己所需的格式
    for e in yaml_datas['datas']:
        if e['level'] == level:
            print(e)
            datas.append([e['a'], e['b'], e['except']])
            ids.append(e['describe'])
    print(datas)
    print(ids)
    return [datas, ids]


@allure.feature("计算器相加功能")
class TestAdd(Base):

    # 将yaml中P0的作为参数传入用例
    @pytest.mark.P0
    @pytest.mark.parametrize(
        "a,b,expect", get_add_datas('P0')[0], ids=get_add_datas('P0')[1]
    )
    @allure.story("P0_相加功能用例")
    def test_add_P0(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            result = self.cals.add(a, b)
        logger.info(f"实际结果为：{result}")
        with allure.step(f"断言{result} == {expect}"):
            assert result == expect

    # 将yaml中P1的作为参数传入用例
    @pytest.mark.P1
    @pytest.mark.parametrize(
        "a,b,expect", get_add_datas('P1')[0], ids=get_add_datas('P1')[1]
    )
    @allure.story("相加功能用例")
    def test_add_P1(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            with pytest.raises(NameError) as exc_info:
                self.cals.add(a, b)
                raise NameError("参数大小超出范围")
                logger.info(f"实际结果为：{exc_info}")
            with allure.step(f"断言{exc_info.value.args[0]} == {expect}"):
                assert exc_info.value.args[0] == expect

    # 将yaml中P2的作为参数传入用例
    @pytest.mark.P2
    @pytest.mark.parametrize(
        "a,b,expect", get_add_datas('P2')[0], ids=get_add_datas('P2')[1]
    )
    @allure.story("P2_级别相加功能用例")
    def test_add_P2(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            # ？？？？？此处异常处理eva意义？？？？？？？
            with pytest.raises(eval(expect)) as e:
                result = self.cals.add(a, b)
                logger.info(f"实际结果为：{result}")
                logger.info(f"错误类型应未：{e}")
            assert e.type == TypeError

    '''
    当前不存在P3级别用例数据，此处只是为了增加使用跳过、预期失败用例的功能
    @skip(reason="XXXX')
    @skipif
    @xfail
    '''

    @pytest.mark.skip(reason="当前不存在P3级别用例数据")
    @pytest.mark.P3
    @pytest.mark.parametrize(
        "a,b,expect", get_add_datas('P3')[0], ids=get_add_datas('P3')[1]
    )
    @allure.story("P3_级别相加功能用例")
    def test_add_P3(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            with pytest.raises(eval(expect)) as e:
                result = self.cals.add(a, b)
                logger.info(f"实际结果为：{result}")
                logger.info(f"错误类型应未：{e}")
            assert e.type == TypeError
