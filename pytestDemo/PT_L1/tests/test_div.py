import os

import allure
import pytest

from PT_L1.Base.base import Base
from PT_L1.util.util import Util
from PT_L1.util.log_util import logger


def get_div_datas():
    # 当前文件路径
    root_path = os.path.dirname(os.path.abspath(__file__))
    # 针对当前目录拼接yaml文件目录
    yaml_path = os.sep.join([root_path, '..', 'datas\divData.yaml'])
    # 调用读取yaml文件方法
    yaml_datas = Util.get_yaml_data(yaml_path)
    datas = []
    ids = []
    # 将yaml数据转化为自己所需的格式
    for e in yaml_datas:
        datas.append([yaml_datas[e]['a'], yaml_datas[e]['b'], yaml_datas[e]['except']])
        ids.append(yaml_datas[e]['describe'])
    print(datas)
    print(ids)
    return [datas, ids]


@allure.feature("计算器相除功能")
class TestDiv(Base):
    # 将查出的yaml作为参数传入用例
    @pytest.mark.parametrize(
        "a,b,expect", get_div_datas()[0], ids=get_div_datas()[1]
    )
    def test_div(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        # 此处用的是python的异常处理，下次应优化为pytest的异常处理
        with allure.step("调用相除的方法"):
            try:
                result = self.cals.div(a, b)
            except Exception as e:
                logger.info(f"传入参数不合法，生成异常{e}")
                result = 'error num'
        logger.info(f"实际结果为：{result}")
        with allure.step(f"断言{result} == {expect}"):
            assert result == expect
