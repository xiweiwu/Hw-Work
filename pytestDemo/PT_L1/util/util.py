import yaml


class Util:
    @classmethod
    def get_yaml_data(cls, fileName):
        '''
        :param fileName: 读取yaml文件的文件路径
        :return: 返回yaml文件的值
        '''
        with open(fileName,'r',encoding='utf-8') as f:
            datas = yaml.safe_load(f)
        return datas