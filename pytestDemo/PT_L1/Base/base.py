from PT_L1.script.calculator import Calculator
from PT_L1.util.log_util import logger


class Base:
    def setup_class(self):
        self.cals = Calculator()

    def setup(self):
        logger.info("开始计算")

    def teardown(self):
        logger.info("结束计算")

    def teardown_class(self):
        logger.info("=======结束测试======")
