import os.path

import pytest
import yaml





class Utils:
    @classmethod
    def get_yaml_data(cls, fileName):
        '''
        :param fileName: 读取yaml文件的文件路径
        :return: 返回yaml文件的值
        '''
        with open(fileName, 'r', encoding='utf-8') as f:
            datas = yaml.safe_load(f)
        return datas

    @classmethod
    def get_add_datas(cls,level):
        '''

        :return:
        '''
        # 当前文件路径
        root_path = os.path.dirname(os.path.abspath(__file__))
        # 针对当前目录拼接yaml文件目录
        yaml_path = os.sep.join([root_path, '..', 'datas\clsAddData.yaml'])
        # 调用读取yaml文件方法
        yaml_datas = Utils.get_yaml_data(yaml_path)
        datas = []
        ids = []
        # 将yaml数据转化为自己所需的格式
        for e in yaml_datas['datas']:
            if e['level'] == level:
                print(e)
                datas.append([e['a'], e['b'], e['except']])
                ids.append(e['describe'])
        return [datas, ids]
    @classmethod
    def get_add_case(cls,level):
        '''

        :return:
        '''
        # 当前文件路径
        root_path = os.path.dirname(os.path.abspath(__file__))
        # 针对当前目录拼接yaml文件目录
        yaml_path = os.sep.join([root_path, '..', 'datas\clsAddData.yaml'])
        # 调用读取yaml文件方法
        yaml_datas = Utils.get_yaml_data(yaml_path)
        datas = []
        # 将yaml数据转化为自己所需的格式
        for e in yaml_datas['datas']:
            if e['level'] == level:
                datas.append([e['a'], e['b'], e['except']])
        return datas

    @classmethod
    def get_div_datas(cls,level):
        # 当前文件路径
        root_path = os.path.dirname(os.path.abspath(__file__))
        # 针对当前目录拼接yaml文件目录
        yaml_path = os.sep.join([root_path, '..', 'datas\divData.yaml'])
        # 调用读取yaml文件方法
        yaml_datas = Utils.get_yaml_data(yaml_path)
        print(yaml_datas)
        datas = []
        ids = []
        # 将yaml数据转化为自己所需的格式
        for e in yaml_datas['datas']:
            if e['level'] == level:
                print(e)
                datas.append([e['a'], e['b'], e['except']])
                ids.append(e['describe'])
        print(datas)
        print(ids)
        return [datas, ids]


