import os

import allure
import pytest

from PT_L3.Base.base import Base
from PT_L3.utils.utils import Utils
from PT_L3.utils.log_util import logger

'''
解析yaml用例，为用例参数化做准备
level：用例等级，当前用例只有P0、P1、P2
'''




@allure.feature("计算器相除功能")
class TestDiv(Base):
    '''
    @pytest.mark.P0：做P0用例的标记
    @allure.story：allure报告中添加用例标题
    @pytest.mark.parametrize：参数化yaml中P0基本的测试数据
    with allure.step：allure添加用例盘
    assert：验证实际结果与预期世界是否一致
    '''
    @pytest.mark.P0
    @allure.story("P0_相加功能用例")
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_div_datas('P0')[0], ids=Utils.get_div_datas('P0')[1]
    )
    def test_div_P0(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相除的方法"): # allure添加用例步骤
            result = self.cals.div(a, b)
        logger.info(f"实际结果为：{result}")
        with allure.step(f"断言{result} == {expect}"):
            assert result == expect


    '''
    @pytest.mark.P1：做P1用例的标记
    @allure.story：allure报告中添加用例标题
    @pytest.mark.parametrize：参数化yaml中P1基本的测试数据
    with allure.step：allure添加用例盘
    assert：验证实际结果与预期世界是否一致
    '''
    @pytest.mark.P1
    @allure.story("P1_相加功能用例")
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_div_datas('P1')[0], ids=Utils.get_div_datas('P1')[1]
    )
    def test_div_P1(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相除的方法"):
            with pytest.raises(NameError) as exc_info:
                self.cals.div(a, b)
                raise NameError("参数大小超出范围")
                logger.info(f"实际结果为：{exc_info}")
            with allure.step(f"断言{exc_info.value.args[0]} == {expect}"):
                assert exc_info.value.args[0] == expect

    '''
    @pytest.mark.P2：做P2用例的标记
    @allure.story：allure报告中添加用例标题
    @pytest.mark.parametrize：参数化yaml中P2基本的测试数据
    with allure.step：allure添加用例盘
    assert：验证实际结果与预期世界是否一致
    pytest.skip：当除数为0 异常 ZeroDivisionError 跳过此步
    '''
    @pytest.mark.P2
    @allure.story("P2_相加功能用例")
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_div_datas('P2')[0], ids=Utils.get_div_datas('P2')[1]
    )
    def test_div_P2(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相除的方法"):
            with pytest.raises(eval(expect)) as e:
                result = self.cals.div(a, b)
                logger.info(f"实际结果为：{result}")
                logger.info(f"错误类型应未：{e}")
            if e.type == ZeroDivisionError :
                pytest.skip("除数不能为0")
            else:
                assert e.type == TypeError

