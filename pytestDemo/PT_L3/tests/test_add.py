"""
用于定义测试加法的测试类
"""
import os.path
import allure
import pytest
from PT_L3.Base.base import Base
from PT_L3.utils.utils import Utils
from PT_L3.utils.log_util import logger


@allure.feature("计算器相加功能")
class TestAdd(Base):
    '''
    获取yaml文件中的数据
    '''

    # 将yaml中P0的作为参数传入用例
    @pytest.mark.P0
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_add_datas('P0')[0], ids=Utils.get_add_datas('P0')[1]
    )
    @allure.story("P0_相加功能用例")
    def test_add_P0(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            result = self.cals.add(a, b)
        logger.info(f"实际结果为：{result}")
        with allure.step(f"断言{result} == {expect}"):
            assert result == expect

    # 直接传入fixture：get_p1_case 方法读取其中的参数 实现参数化

    '''
     传入yaml文件中的数据，存入fixture中,进行参数化
     '''
    @pytest.fixture(params=Utils.get_add_case('P1'))
    def get_p1_case(self, request):
        print(f"-------------?{request.param}")
        return request.param

    @pytest.mark.P1
    @allure.story("相加功能用例")
    def test_add_P1(self, get_p1_case):
        a, b, expect = get_p1_case[0], get_p1_case[1], get_p1_case[2]
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            with pytest.raises(NameError) as exc_info:
                self.cals.add(a, b)
                raise NameError("参数大小超出范围")
                logger.info(f"实际结果为：{exc_info}")
            with allure.step(f"断言{exc_info.value.args[0]} == {expect}"):
                assert exc_info.value.args[0] == expect

    # 将yaml中P2的作为参数传入用例
    @pytest.mark.P2
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_add_datas('P2')[0], ids=Utils.get_add_datas('P2')[1]
    )
    @allure.story("P2_级别相加功能用例")
    def test_add_P2(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            # ？？？？？此处异常处理eva意义？？？？？？？
            with pytest.raises(eval(expect)) as e:
                result = self.cals.add(a, b)
                logger.info(f"实际结果为：{result}")
                logger.info(f"错误类型应未：{e}")
            assert e.type == TypeError

    '''
    当前不存在P3级别用例数据，此处只是为了增加使用跳过、预期失败用例的功能
    @skip(reason="XXXX')
    @skipif
    @xfail
    '''

    @pytest.mark.skip(reason="当前不存在P3级别用例数据")
    @pytest.mark.P3
    @pytest.mark.parametrize(
        "a,b,expect", Utils.get_add_datas('P3')[0], ids=Utils.get_add_datas('P3')[1]
    )
    @allure.story("P3_级别相加功能用例")
    def test_add_P3(self, a, b, expect):
        logger.info(f"a={a},b={b},except={expect}")
        with allure.step("调用相加的方法"):
            with pytest.raises(eval(expect)) as e:
                result = self.cals.add(a, b)
                logger.info(f"实际结果为：{result}")
                logger.info(f"错误类型应未：{e}")
            assert e.type == TypeError
