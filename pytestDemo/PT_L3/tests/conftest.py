
from PT_L3.utils.log_util import logger
import pytest

@pytest.fixture(scope="function",autouse=True)
def case_run():
    logger.info("开始计算-----------》")
    yield
    logger.info("结束计算------------》")


@pytest.fixture(scope="module",autouse=True)
def clas_fixture():
    logger.info("=======开始测试计算器功能======")
    yield
    logger.info("=======完成测试计算器功能======")



