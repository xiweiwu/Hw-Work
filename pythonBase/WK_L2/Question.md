项目简介
随着学校的规模变大，对应的学员回越来越多，相应的管理越来越难。 学员信息管理系统主要是对学员的各种信息进行管理，能够让学员的信息关系变得科学化、系统化和规范化。

知识点
实体类
成员变量属性
方法
Python 类型注解
Python 数据类dataclas
python封装与property装饰器
受众
中级测试工程师
作业内容
编写学员实体类，对应属性包含：学号、姓名、性别。
编写学员名单管理类，实现删除学员方法、查询学员方法。
学员实体类添加一个私有属性成绩，要求实现对应的 getter 和 setter。
实现更新学员、添加学员操作。
示例代码
根据示例代码中的注释信息，完成此题目的代码逻辑。

class Student:
    """
    自己根据题目要求实现
    """
    pass


class StudentList:
    def __init__(self, student_list: List[Student]):
        self.s_list = student_list

    def get(self, student_id: int):
        """
        根据 student_id 查询信息
        """
        pass

    def delete(self, student_id: int):
        """
        根据 student_id 删除信息
        """
        pass

    def update(self, student: Student):
        pass

    def save(self, student: Student):
        pass


if __name__ == '__main__':
    # 入参自己定义
    s1 = Student()
    s2 = Student()
    s3 = Student()
    # 初始化一个成员名单
    s_list = StudentList([s1, s2, s3])
    #实现save
    s_list.save()
    # 实现update
    s_list.update()
    # 实现get()方法
    s_list.get()
    # 实现delete
    s_list.delete()