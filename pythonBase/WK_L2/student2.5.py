from dataclasses import dataclass,field

@dataclass
class Student:

    """
     #私有属性，成绩默认为0
    __record =0
       #定义构造方法
        def __init__(self,id:int,name:str,sex:str):
        #实例属性
        self.id=id
        self.name=name
        self.sex=sex
    """
    # 用dataclas代替构造方法
    id:int
    name:str
    sex:str
    # __record:int=0
    __record: int = field(default=0)

    @property
    def record(self):
        return self.__record
    @record.setter
    def record(self,record):
        if(record>100) or (record<0):
            print("成绩只能输入：0-100")
        else:
            self.__record=record

    def __str__(self):
        return f"id：{self.id}，name：{self.name}，sex：{self.sex},record：{self.record}"

@dataclass
class StudentList:
    """
        def __init__(self,s_list):
        self.s_list = s_list
    """
    #用dataclas代替构造方法
    s_list:dict

    def manageStudent(self):
        if len(self.s_list)>0:
            print("---------------学员信息汇总---------------------")
            for a in self.s_list:
                print(self.s_list[a])
        else:
            print("学生列表为空，请先录入学员信息")

    def get(self, student_id):
        if student_id in self.s_list:
            stu = self.s_list.get(student_id)
            print(f"存在{student_id}的成员，该成员信息为：")
            print(stu)
        else:
            print(f"不存在{student_id}的成员")

    def delete(self, student_id):
        if student_id in self.s_list:
            stu= self.s_list.pop(student_id)
            print(f"存在{student_id}的成员，删除成员信息为：")
            print(stu)
        else:
            print(f"不存在{student_id}的成员")


    def update(self, student: Student):
        try:
            if student.id in self.s_list:
                self.s_list.update({student.id: student})
            else:
                print(f"不存在的成员,无法更新信息")
        except Exception as e:
            print(f"用户更新失败，用户type：{type(student)}，用户信息：{student}")
            print(f"异常信息提示：{e}")


    def save(self, student: Student):
        try:
            if student.id in self.s_list:
                print(f"已存在该成员,无法新增信息")
            else:
                self.s_list.update({student.id: student})
        except Exception as e:
            print(f"用户新增失败，用户type：{type(student)}，用户信息：{student}")
            print(f"异常信息提示：{e}")

if __name__ == '__main__':
    s_list = StudentList(dict())

    #创建一个循环进行学员的录入
    while True:
        #根据输入选择需要对学员进行的操作
        s_id=input("请输入学员的学号：")
        s_setting = input(f"你希望对id={s_id}学员执行的操作：1-新增；2-更新；3-查询单个学员；4-删除该学员")

        #学员新增或更新时，需要输入学员的所有信息
        if int(s_setting) in (1,2):
            s_name= input("请输入学员的姓名：")
            s_sex = input("请输入学员的性别：")
            s_record = input("请输入学员的成绩：")
            stu=Student(int(s_id),s_name,s_sex,int(s_record))
            '''
             #传入参数不匹配，用来测试异常操作
            stu = (int(s_id), s_name, s_sex, int(s_record))
            '''

            if int(s_setting) == 1:
                s_list.save(stu)
            else:
                s_list.update(stu)
        # 学员新增或更新时，只需提取学员的id
        elif int(s_setting) in (3,4):
            if int(s_setting)==3:
                s_list.get(int(s_id))
            else:
                s_list.delete(int(s_id))
        else:
            print(f"无法识别您所输入的{s_setting}!!!")
        # 询问是否继续执行之前操作，不继续时，打印所有的学员信息
        answer = input('是否继续执行操作？Y:继续，N:不继续')
        if answer == 'Y':
            continue
        else:
            s_list.manageStudent()
            break


