class Student:
    #私有属性，成绩默认为0
    __record =0

    #定义构造方法
    def __init__(self,id:int,name:str,sex:str):
        #实例属性
        self.id=id
        self.name=name
        self.sex=sex
    @property
    def record(self):
        return self.__record
    @record.setter
    def record(self,record):
        if(record>100) or (record<0):
            print("成绩只能输入：0-100")
        else:
            self.__record=record

    def __str__(self):
        return f"id为{self.id},性别为{self.sex}的{self.name},成绩是{self.record}"

class StudentList:
    def __init__(self,s_list):
        self.s_list = s_list


    def manage(self):
        if len(self.s_list)>0:
            for a in self.s_list:
                print(self.s_list[a])
        else:
            print("学生列表为空，请先录入学员信息")

    def get(self, student_id):
        if student_id in self.s_list:
            stu = self.s_list.get(student_id)
            print(f"存在{student_id}的成员，该成员信息为：")
            print(stu)
        else:
            print(f"不存在{student_id}的成员")

    def delete(self, student_id):
        if student_id in self.s_list:
            stu= self.s_list.pop(student_id)
            print(f"存在{student_id}的成员，删除成员信息为：")
            print(stu)
        else:
            print(f"不存在{student_id}的成员")


    def update(self, student: Student):
        if student.id in self.s_list:
            self.s_list.update({student.id:student})
        else:
            print(f"不存在的成员,无法更新信息")

    def save(self, student: Student):
        if student.id in self.s_list:
            print(f"已存在该成员,无法新增信息")
        else:
            self.s_list.update({student.id: student})


if __name__ == '__main__':
    # 创建学员对象
    s1=Student(1, "李四", "女")
    s2=Student(2,"王小花","女")
    s3=Student(3,"刘水水","男")
    #传入学员成绩
    s1.record=60
    s2.record = 49
    s3.record = 99
    # 初始化一个成员名单
    s_list = StudentList({s1.id: s1, s2.id: s2, s3.id: s3})

    # # 实现get()方法
    s_list.get(1)
    s_list.get(2)
    s_list.get(3)
    # # 实现delete
    s_list.delete(2)

    #更新成员
    s4 = Student(1, "王一一", "男")
    s5 = Student(2, "刘", "女")
    s_list.update(s4)
    s_list.update(s5)

    #添加新成员
    s_list.save(s4)
    s_list.save(s5)

    #查看当前所有学员
    s_list.manage()

