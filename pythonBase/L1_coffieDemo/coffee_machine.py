'''
https://ceshiren.com/t/topic/24527
'''
class Coffee:

    def __init__(self,name:str,price:int,formula:str):
        self.name=name
        self.price=price
        self.formula=formula

    def __str__(self):
        return  f'{self.name}({self.price}元): {self.formula}'

class Americao(Coffee):
    def __init__(self):
        super().__init__('美式', 15, '浓缩咖啡')

class Latte(Coffee):
    def __init__(self):
        super().__init__('拿铁', 20, '浓缩咖啡，牛奶，糖')

class CoffeeMachine:
    def __init__(self):
        self.products=[Latte(),Americao()]

    def coffeeDisplay(self):
        print("------ 展示咖啡 ------")
        for coffee in self.products:
            print(coffee)

    def coffeeChoose(self)-> Coffee:
        print("------ 选择咖啡 ------")
        while True:
            selected = input('请输入数字选择咖啡 0：拿铁，1：美式咖啡')

            if selected in ('0', '1'):
                return self.products[int(selected)]
            else:
                print('输入不合法，请重新选择可选的咖啡。')
                continue

    def coffeePay(self,**chooseCoffee)->int:
        print("------ 投入钱币 ------")
        tomo=0
        # for i in range(0, len(coffees)):
        # for coffee in coffees:
        #     tomo+=coffee.price
        for coffee in chooseCoffee:
            if coffee.key in self.products:
                tomo=


        while True:
            pay_mo = input(f'请投入{tomo}元')
            money = int(pay_mo)
            if money < tomo:
                print('金额不够，请重新投币。原金额已经退回')
                break
            else:
                money=money-tomo
                return money
        # """输入钱币"""
        # print('------ 输入钱币 ------')
        # while True:
        #     money = input(f'请投入{coffee.price}元')
        #     if int(money) < coffee.price:
        #         print('金额不够，请重新投币。原金额已经退回')
        #         continue
        #     else:
        #         return int(money)

    def coffeeMake(self,coffees:Coffee):
        print("------ 制作咖啡 ------")
        print("制作中，请稍候......")
        for coffee in coffees:
            print(f"{coffee.name}制作完成！")

    def coffeeRecord(self):
        print("------ 打印记录 ------")


if __name__=='__main__':
    coffeeMaine=CoffeeMachine()
    choose_Coffees= {}
    while True:
        #展示咖啡
        coffeeMaine.coffeeDisplay()
        #选择咖啡
        choose_Coffee=coffeeMaine.coffeeChoose()
        if choose_Coffee.name in choose_Coffees.keys():
            choose_Coffees[choose_Coffee.name]+1
        else:
            choose_Coffees.update({choose_Coffee.name:1})


        # 询问是否继续购买
        answer=input('是否继续购买？Y:继续，N:不继续')
        if answer == 'Y':
            continue
        else:
            break

    # 投入铅笔
    pay_Money = coffeeMaine.coffeePay(choose_Coffees)
    # 制作咖啡
    coffeeMaine.coffeeMake(choose_Coffees)
    #订单记录
    coffeeMaine.coffeeRecord()