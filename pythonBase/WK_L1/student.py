class Student:


    #定义学生属性，id、姓名、性别
    student_id = 0
    student_name ="张三"
    student_sex= '男'

    #定义构造方法
    def __init__(self,id,name,sex):
        #实例属性
        self.student_id=id
        self.student_name=name
        self.student_sex=sex
        print(f"这是id为{id},性别为{sex}的{name}",self.student_id,self.student_sex,self.student_name)




class StudentList:
    s_list={}
    def __init__(self, student_list):
        self.s_list = student_list
        print(self.s_list.items())

    def get(self, student_id):
        stu=self.s_list.get(student_id)
        print(f"存在{student_id}的成员")



    def delete(self, student_id):
        stuLi2=self.s_list.pop(student_id)
        print(f"存在{student_id}的成员")


if __name__ == '__main__':
    # 入参自己定义
    s1 = Student(1,"李四","女")
    s2 = Student(2,"王小花","女")
    s3 = Student(3,"刘水水","男")

    # 初始化一个成员名单
    s_list = StudentList({s1.student_id:s1,s2.student_id:s2,s2.student_id:s2})
    # 实现get()方法
    s_list.get(1)
    # 实现delete
    s_list.delete(2)
