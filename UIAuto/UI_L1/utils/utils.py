import os.path

import pytest
import yaml


class Utils:
    @classmethod
    def get_yaml_data(cls, fileName):
        '''
        :param fileName: 读取yaml文件的文件路径
        :return: 返回yaml文件的值
        '''
        with open(fileName, 'r', encoding='utf-8') as f:
            datas = yaml.safe_load(f)
        return datas

    @classmethod
    def get_search_datas(cls, level):
        # 当前文件路径
        root_path = os.path.dirname(os.path.abspath(__file__))
        # 针对当前目录拼接yaml文件目录
        yaml_path = os.sep.join([root_path, '..', 'datas\searchData.yaml'])
        # 调用读取yaml文件方法
        yaml_datas = Utils.get_yaml_data(yaml_path)
        datas = []
        # 将yaml数据转化为自己所需的格式
        for e in yaml_datas[level]:
            datas.append([e])
        print(f"------------>getdate-------{level}------>{datas}")
        return datas

    # def get_search_datas(level):
    #     # 当前文件路径
    #     root_path = os.path.dirname(os.path.abspath(__file__))
    #     # 针对当前目录拼接yaml文件目录
    #     yaml_path = os.sep.join([root_path, '..', 'datas\searchData.yaml'])
    #     # 调用读取yaml文件方法
    #     yaml_datas = Utils.get_yaml_data(yaml_path)
    #     print(f"------------>{yaml_datas['P0']}")
    #     return yaml_datas[level]
    #
    # # P0级别测试数据
    # @pytest.fixture(params=get_search_datas('P0'))
    # def get_p0_search_caseData(request):
    #     return request.param
    #
    # # P1级别测试数据
    # @pytest.fixture(params=get_search_datas('P1'))
    # def get_p1_search_caseData(request):
    #     return request.param
    #
    # # P2级别测试数据
    # @pytest.fixture(params=get_search_datas('P1'))
    # def get_p1_search_caseData(request):
    #     return request.param
