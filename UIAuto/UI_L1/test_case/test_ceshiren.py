"""
测试用例编写
"""
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from UIAuto.UI_L1.utils.utils import Utils
class Test_Ceshiren:

    """
    测试搜索页面的前置动作：
    1.打开浏览器强制等待
    """
    def setup_class(self):
        # 打开浏览器
        # self.driver=webdriver.Chrome(executable_path="D:\Driver\chromedriver_win32\chromedriver.exe")
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3)

    """
        用例前置动作：
        为了保证用例不相互影响，每次执行用例前重新打开被测地址
    """
    def setup(self):
        # 打开被测地址
        self.driver.get("https://ceshiren.com/search?expanded=true")

    #每一次用例结束后都会关闭浏览器
    def teardown_class(self):
        self.driver.quit()

    # P0级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P0'))
    def get_p0_search_caseData(self,request):
        return request.param
    # P1级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P1'))
    def get_p1_search_caseData(self,request):
        return request.param
    # P2级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P1'))
    def get_p2_search_caseData(self,request):
        return request.param


    """
    测试搜素功能 demo ：
        前提条件：打开测试人搜素页面
        测试步骤：
            搜索文本框中输入搜索关键字
            点击搜索按钮
        预期结果：
            搜索出来的结果包含输入的关键词
    """
    def test_search(self):
        #在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR,"[placeholder='搜索']").send_keys("selenium")
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        #断言预期与实际结果
        #获取实际结果(单条)
        web_element=self.driver.find_element(By.CLASS_NAME,"topic-title").text
        assert "selenium" in web_element.lower() #将获取到的内容全部转换为小写
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements: #print(f"--------------->{e.text}")
            assert "selenium" in e.text.lower()

    """
    P0优先级测试用例：验证正常情况 查询结果
    引入fixture参数化，yaml文件中p0
    """
    def test_search_P0(self,get_p0_search_caseData):
        keyword=get_p0_search_caseData[0]
        #在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR,"[placeholder='搜索']").send_keys(keyword)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements:
            assert keyword in e.text.lower()



    """
    P1优先级测试用例：验证正常情况 查询结果
    """
    def test_search_P1(self,get_p1_search_caseData):
        keyword=get_p1_search_caseData[0]
        #在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR,"[placeholder='搜索']").send_keys(keyword)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements:
            assert keyword in e.text.lower()

    """
        P1优先级测试用例：验证正常情况 查询结果
        """

    def test_search_P2(self, get_p2_search_caseData):
        keyword = get_p2_search_caseData[0]
        # 在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='搜索']").send_keys(keyword)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements:
            assert keyword in e.text.lower()