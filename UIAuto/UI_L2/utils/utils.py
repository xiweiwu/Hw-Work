import os.path
import time

import allure
import yaml
from selenium import webdriver


class Utils:
    @classmethod
    def get_yaml_data(cls, fileName):
        '''
        :param fileName: 读取yaml文件的文件路径
        :return: 返回yaml文件的值
        '''
        with open(fileName, 'r', encoding='utf-8') as f:
            datas = yaml.safe_load(f)
        return datas

    @classmethod
    def get_search_datas(cls, level):
        # 当前文件路径
        root_path = os.path.dirname(os.path.abspath(__file__))
        # 针对当前目录拼接yaml文件目录
        yaml_path = os.sep.join([root_path, '..', f'datas/searchData.yaml'])
        # 调用读取yaml文件方法
        yaml_datas = Utils.get_yaml_data(yaml_path)
        datas = []
        # 将yaml数据转化为自己所需的格式
        for e in yaml_datas[level]:
            datas.append([e])
        print(f"------------>getdate-------{level}------>{datas}")
        return datas

    @classmethod
    def get_screen(cls, dr):
        timestamp = int(time.time())
        image_path = f"../result/image/image_{timestamp}.PNG"
        dr.save_screenshot(image_path)
        allure.attach.file(image_path, name="picture", attachment_type=allure.attachment_type.PNG)

    @classmethod
    def get_pageSource(cls, dr):
        htmltamp = int(time.time())
        html_path = f"../result/pageSources/pageSource_{htmltamp}.html"
        with open(html_path,"w",encoding="utf-8") as f:
            f.write(dr.page_source)
        allure.attach.file(html_path, name="html", attachment_type=allure.attachment_type.HTML)
