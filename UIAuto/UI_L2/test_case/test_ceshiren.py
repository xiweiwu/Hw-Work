"""
测试用例编写
"""
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from UIAuto.UI_L2.utils.log_util import logger
from UIAuto.UI_L2.utils.utils import Utils
class Test_Ceshiren:
    """
    测试搜索页面的前置动作：
    1.打开浏览器强制等待
    """
    def setup_class(self):
        # 打开浏览器
        # self.driver=webdriver.Chrome(executable_path="D:\Driver\chromedriver_win32\chromedriver.exe")
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(3)
        # 打开被测地址
        self.driver.get("https://ceshiren.com")
        logger.debug(f"打开测试人论坛：https://ceshiren.com")
        #获取测试人论坛截图
        Utils.get_screen(self.driver)
        #=========通过页面点击，跳转高级搜索页面============
        #点击搜索按钮打开搜索弹窗
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable((By.ID, "search-button")))
        self.driver.find_element(By.ID, "search-button").click()
        #打开搜索弹窗，查找高级搜索按钮加载完成
        WebDriverWait(self.driver,10).until(
            expected_conditions.element_to_be_clickable((By.CSS_SELECTOR,'.panel-body .searching')))
        #点击高级搜索按钮，进入高级搜索页面
        self.driver.find_element(By.CSS_SELECTOR,".panel-body .searching").click()
        logger.debug(f"点击搜索弹窗的高级搜索按钮，进入高级搜索页面")
        # =========通过页面点击，跳转高级搜索页面完成============
    """
        用例前置动作：
        为了保证用例不相互影响，每次执行用例前重新打开被测地址
    """
    def setup(self):
        #依据高级搜索页面，搜索按钮是否点击判断页面加载成功
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, '.search-cta')))

        logger.debug(f"高级搜索页面加载完成")
        #高级搜索关键字前页面截图
        Utils.get_screen(self.driver)
    def teardown(self):
        # 每次用例执行完成后，搜索结果截图截图
        Utils.get_screen(self.driver)
        #清除搜索记录，为下一次搜索做准备
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='搜索']").clear()
        logger.debug(f"用例数据清除")
        # Utils.get_screen(self.driver)

    #每一次模块用例结束后都会关闭浏览器
    def teardown_class(self):
        self.driver.quit()

    # P0级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P0'))
    def get_p0_search_caseData(self,request):
        return request.param
    # P1级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P1'))
    def get_p1_search_caseData(self,request):
        return request.param
    # P2级别测试数据
    @pytest.fixture(params=Utils.get_search_datas('P1'))
    def get_p2_search_caseData(self,request):
        return request.param

    """
    P0优先级测试用例：验证正常情况 查询结果
    引入fixture参数化，yaml文件中p0
    """

    def test_search_P0(self, get_p0_search_caseData):
        keyword = get_p0_search_caseData[0]
        # 在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='搜索']").send_keys(keyword)
        logger.info(f"输入关键词进行搜索:{keyword}")
        # 在搜索弹窗中输入关键字截图
        Utils.get_screen(self.driver)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_all_elements_located(
                (By.CSS_SELECTOR, '.search-results .topic-title')))
        web_element = self.driver.find_element(By.CLASS_NAME, "topic-title").text
        logger.info(f"搜索结果的第一个标题为:{web_element}")
        logger.info(f"忽略大小写,对关键词:{keyword}和实际结果:{web_element}进行断言")
        #获取页面的pagesource
        Utils.get_pageSource(self.driver)
        assert keyword.lower() in web_element.lower()  # 将获取到的内容全部转换为小写


    """
    P1优先级测试用例：验证正常情况 查询结果
    """
    def test_search_P1(self,get_p1_search_caseData):
        keyword=get_p1_search_caseData[0]
        #在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='搜索']").send_keys(keyword)
        logger.info(f"输入关键词进行搜索:{keyword}")
        # 在搜索弹窗中输入关键字截图
        Utils.get_screen(self.driver)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_all_elements_located(
                (By.CSS_SELECTOR, '.search-results .topic-title')))

        # 断言预期与实际结果
        # 获取实际结果(多条)
        # web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements:
            logger.info(f"搜索结果的标题为:{e.text}")
            logger.info(f"忽略大小写,对关键词:{keyword}和实际结果:{e.text}进行断言")
            assert (keyword.lower()) in e.text.lower()

    """
    P2优先级测试用例：验证正常情况 查询结果
    """

    def test_search_P2(self, get_p2_search_caseData):
        keyword = get_p2_search_caseData[0]
        # 在搜索弹窗中输入关键字
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='搜索']").send_keys(keyword)
        logger.info(f"输入关键词进行搜索:{keyword}")
        # 在搜索弹窗中输入关键字截图
        Utils.get_screen(self.driver)
        # 点击搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".search-cta").click()
        # 断言预期与实际结果
        # 获取实际结果(多条)
        web_elements = WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_all_elements_located(
                (By.CSS_SELECTOR, '.search-results .topic-title')))

        # 断言预期与实际结果
        # 获取实际结果(多条)
        # web_elements = self.driver.find_elements(By.CLASS_NAME, "topic-title")
        for e in web_elements:
            logger.info(f"搜索结果的标题为:{e.text}")
            logger.info(f"忽略大小写,对关键词:{keyword}和实际结果:{e.text}进行断言")
            assert (keyword.lower()) in e.text.lower()