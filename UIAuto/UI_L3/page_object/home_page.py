"""
进入首页
1.点击谭家成员
通讯录页面
"""
from selenium.webdriver.common.by import By

from UIAuto.UI_L3.utils.log_utils import logger
from UIAuto.UI_L3.page_object.base_page import BasePage


class HomePage(BasePage):
    __BTN_ADD_MEMBER = (By.LINK_TEXT, '添加成员')

    def click_add_member_btn(self):
        logger.info("点击添加成员按钮")

        # 点击添加成员，添加成员页面
        # self.driver.find_element(self.__BTN_ADD_MEMBER[0],self.__BTN_ADD_MEMBER[1]).click()
        # 解包方式传参
        self.do_find(*self.__BTN_ADD_MEMBER).click()

        from UIAuto.UI_L3.page_object.member_page import MemberPage
        return MemberPage(self.driver)
