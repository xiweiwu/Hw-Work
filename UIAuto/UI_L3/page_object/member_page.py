"""
1.添加成员，输入成员信息
2.点击保存
显示保存成功
回到通讯录首页
1.断言通讯录信息是否与添加信息一致。
"""
from selenium.webdriver.common.by import By

from UIAuto.UI_L3.utils.log_utils import logger
from UIAuto.UI_L3.page_object.base_page import BasePage


class MemberPage(BasePage):
    """填写成员信息页面"""
    __INPUT_USERNAME = By.ID, "username"
    __INPUT_ACCTID = By.ID,"memberAdd_acctid"
    __INPUT_MOBILE = By.NAME, "mobile"
    __BTN_SAVE = By.CSS_SELECTOR, ".qui_btn.ww_btn.js_btn_save"

    def fill_out_info(self, username, acctid, mobile):
        logger.info("填写新成员信息")
        '''
        填写信息
        '''

        # 输入添加成员信息，点击添加按钮
        self.do_find(*self.__INPUT_USERNAME).send_keys(username)
        self.do_find(*self.__INPUT_ACCTID).send_keys(acctid)
        self.do_find(*self.__INPUT_MOBILE).send_keys(mobile)
        self.do_find(*self.__BTN_SAVE).click()

        from UIAuto.UI_L3.page_object.contants_page import ContantsPage
        return ContantsPage(self.driver)
