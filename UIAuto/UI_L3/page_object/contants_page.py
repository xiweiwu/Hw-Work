from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from UIAuto.UI_L3.utils.log_utils import logger
from UIAuto.UI_L3.page_object.base_page import BasePage


class ContantsPage(BasePage):
    """通讯录列表页面"""
    __TIPS = By.ID, 'js_tips'

    def get_tips(self):
        logger.info("获取冒泡消息")
        '''通讯录列表页获取冒泡消息'''

        # 断言信息提示成员添加成功
        # tips_loc = By.ID, 'js_tips'
        #
        # WebDriverWait(self.driver, 10, 2).until(
        #     expected_conditions.visibility_of_element_located(tips_loc))
        self.waite_ele_util_visible(self.__TIPS)

        tips_text = self.driver.find_element(*self.__TIPS).text
        return tips_text
