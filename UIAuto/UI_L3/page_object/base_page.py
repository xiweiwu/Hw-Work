from datetime import time

from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    _BASE_URL = ''

    def __init__(self, base_driver=None):

        # 浏览器的实例化
        if base_driver:
            self.driver = base_driver
        else:
            # 实例化浏览器
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(15)
            self.driver.maximize_window()

        if not self.driver.current_url.startswith("http"):
            self.driver.get(self._BASE_URL)

    # 定位方式的封装
    def do_find(self, by, locator: str):
        """获取单个元素"""
        element = self.driver.find_element(by, locator)
        return element
        # if locator:
        #     return self.driver.find_element(by, locator)
        # else:
        #     return self.driver.find_element(*by)

    def do_finds(self, by, locator: str):
        """获取多个元素"""
        # if locator:
        #     return self.driver.find_elements(by, locator)
        # else:
        #     return self.driver.find_elements(*by)
        element = self.driver.find_elements(by, locator)
        return element

    def waite_ele_util_visible(self, locator: tuple):
        WebDriverWait(self.driver, 10, 2).until(
            expected_conditions.visibility_of_element_located(locator)
        )

    # def do_send_keys(self,value, by, locator=None):
    #     """获取多个元素"""
    #     ele=self.do_find(by,locator)
    #     ele.clear()
    #     ele.send_keys(value)
    #
    def do_quite(self):
        self.driver.quit()
