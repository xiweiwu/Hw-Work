"""
1.加入浏览器复用，cookie复用功能
进入首页
"""

import yaml

from UIAuto.UI_L3.utils.log_utils import logger
from UIAuto.UI_L3.page_object.base_page import BasePage


class LoginPage(BasePage):
    _BASE_URL = 'https://work.weixin.qq.com/wework_admin/frame'

    def login(self):
        # 登录
        logger.info("扫码登录")
        #1、访问企业微信首页Domain

        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2、获取本地的cookie
        with open("../datas/cookie.yaml", "r") as f:
            cookies = yaml.safe_load(f)
        # 3、植入cookie
        for ck in cookies:
            self.driver.add_cookie(ck)
        # 4、访问企业微信首页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")


        from UIAuto.UI_L3.page_object.home_page import HomePage
        return HomePage(self.driver)
