import time

import yaml
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait



class TestContantList:


    def test_login(self):
        self.driver=webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome")
        self.driver.implicitly_wait(3)
        #等待时间内，进行人工扫码操作
        # time.sleep(20)
        cookies=self.driver.get_cookies()
        with open("../datas/cookie.yaml", "w") as f:
            yaml.safe_dump(cookies,f)

        # cookies = yaml.safe_load(open("../datas/cookie.yaml"))
        # for cookie in cookies:
        #     self.driver.add_cookie(cookie)
        # time.sleep(3)
        # self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")

        """--------------点击添加成员"""
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, ".index_stage .index_service .ww_indexImg_AddMember")))

        self.driver.find_element(By.CSS_SELECTOR, ".index_service .ww_indexImg_AddMember").click()

        """--------------点击输入成员值"""
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, "[placeholder='姓名']")))

        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='姓名']").send_keys("张三")
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='别名']").send_keys("zhangsan")
        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='成员唯一标识，设定以后不支持修改']").send_keys("zhangsan")
        self.driver.find_element(By.CSS_SELECTOR, "[suffix='@wxwcs1.wecom.work']").clear()
        self.driver.find_element(By.CSS_SELECTOR, "[suffix='@wxwcs1.wecom.work']").send_keys("zhangsan")

        self.driver.find_element(By.CSS_SELECTOR, "[placeholder='成员通过验证该手机后可加入企业']").send_keys("13541360651")
        self.driver.find_element(By.CSS_SELECTOR, ".js_btn_save").click()

        """+++++++++删除数据"""
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, "[title='张三']")))

        self.driver.find_element(By.CSS_SELECTOR, "[title='张三']").click()

        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, ".js_del_member")))

        self.driver.find_element(By.CSS_SELECTOR, ".js_del_member").click()

        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable(
                (By.CSS_SELECTOR, ".qui_dialog .ww_btn_Blue")))

        self.driver.find_element(By.CSS_SELECTOR, ".qui_dialog .ww_btn_Blue").click()

        self.driver.find_element(By.CSS_SELECTOR, ".frame_nav_item_title").click()

        # # self.driver.find_element(By.LINK_TEXT, "邮箱管理").click()
        # self.driver.find_element(By.XPATH, "//* div[@class='weworktool_index_left js_mngtool_index_left']/a[@text='邮箱管理']").click()
        # # // *[ @ id = "main"] / div / div //* div[@class='weworktool_index_left js_mngtool_index_left']/a[@text='邮箱管理']
        #
        # self.driver.find_element(By.XPATH, "//* div[@class='weworktool_index_list_item']/a[@text='前往']").click()
        #
        # WebDriverWait(self.driver, 10).until(
        #     expected_conditions.element_to_be_clickable(
        #         (By.CSS_SELECTOR, ".col_checkbox")))
        #
        # self.driver.find_element(By.CSS_SELECTOR, ".col_checkbox").click()
        # self.driver.find_element(By.CSS_SELECTOR, ".qy_btn js_delAccount").click()

        time.sleep(20)

    #
    # def test_add_cookies(self):
    #     self.dr = webdriver.Chrome()
    #     self.dr.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")
    #     cookies=yaml.safe_load(open("../datas/cookie.yaml"))
    #     for cookie in cookies:
    #         self.dr.add_cookie(cookie)
    #     time.sleep(3)
    #     self.dr.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")

    # def test_memberList(self):
    #     self.driver.refresh()
    #