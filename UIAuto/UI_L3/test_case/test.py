# """
# @Author: 霍格沃兹测试开发学社-西西
# @Desc: 更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860
# """
# import time
#
# import yaml
# from selenium import webdriver
#
#
# # 企业微信的cookie 有互踢机制。
# class TestCookieLogin:
#
#     def setup_class(self):
#         """前置动作"""
#         self.driver = webdriver.Chrome()
#
#     def teardown_class(self):
#         """后置处理"""
#         pass
#         # self.driver.quit()
#
#     def test_save_cookies(self):
#         """获取cookie"""
#
#         # 1、访问企业微信首页
#         self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
#
#         # 2、直接等待，手工扫码
#         time.sleep(10)
#
#         # 3、登录成功后，获取cookie
#         cookies = self.driver.get_cookies()
#
#         # 4、保存cookie
#         with open("./data/cookies.yaml", "w") as f:
#             yaml.safe_dump(data=cookies, stream=f)
#
#     def test_add_cookie(self):
#         """植入cookie"""
#
#         # 1、访问企业微信首页 CookieDomain
#         self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
#
#         # 2、获取本地 cookies
#         with open("./data/cookies.yaml", "r") as f:
#             cookies = yaml.safe_load(f)
#
#         # 3、植入cookies
#         for ck in cookies:
#             self.driver.add_cookie(ck)
#
#         # 4、访问企业微信首页
#         self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
#
#
# # test_contact.py 添加联系人
# """
# @Author: 霍格沃兹测试开发学社-西西
# @Desc: 更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860
# """
# import yaml
# from faker import Faker
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions
# from selenium.webdriver.support.wait import WebDriverWait
#
# from UIAuto.UI_L3.utils.log_utils import logger
#
#
# class TestAddMemberFromHome:
#     def setup_class(self):
#         # mock 数据 姓名，account, 手机号
#         fake: Faker = Faker("zh_CN")
#         self.username = fake.name()
#         self.accid = fake.ssn()
#         self.mobile = fake.phone_number()
#         # 实例化
#         self.driver = webdriver.Chrome()
#         self.driver.implicitly_wait(5)
#         self.driver.maximize_window()
#         logger.info("登录")
#         # 1、访问企业微信首页
#         self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
#         # 2、获取本地的cookie
#         with open("./data/cookies.yaml", "r") as f:
#             cookies = yaml.safe_load(f)
#         # 3、植入cookie
#         for ck in cookies:
#             self.driver.add_cookie(ck)
#         # 4、访问企业微信首页
#         self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
#
#     def teardown_class(self):
#         # 不要退出
#         # self.driver.quit()
#         pass
#
#     def test_add_member(self):
#         # name = "aaab"
#         # acctid = "111112"
#         # phonenum = "13100000001"
#         # 2. 点击添加成员按钮
#         logger.info("点击添加成员按钮")
#         self.driver.find_element(By.CSS_SELECTOR, ".ww_indexImg_AddMember").click()
#         # 3. 填写成员信息
#         logger.info("填写成员信息")
#         # 3.1 输入用户名
#         self.driver.find_element(By.ID, "username").send_keys(self.username)
#         # 3.2 输入acctid
#         self.driver.find_element(By.ID, "memberAdd_acctid").send_keys(self.accid)
#
#         # 3.3 输入手机号
#         self.driver.find_element(By.ID, "memberAdd_phone").send_keys(self.mobile)
#
#         # 3.4 点击保存
#         self.driver.find_element(By.CSS_SELECTOR, ".js_btn_save").click()
#         # 4. 断言结果
#         loc_tips = (By.ID, "js_tips")
#         # 等到可见，再去获取结果文字
#         WebDriverWait(self.driver, 10, 2).until(expected_conditions.visibility_of_element_located(loc_tips))
#         tips_value = self.driver.find_element(*loc_tips).text
#         assert "保存成功" == tips_value
#
#     def test_delete_contact(self):
#         # 删除刚添加的联系人
#         self.driver.find_element(By.XPATH, "//*[text()='通讯录']").click()
#         self.driver.find_element(By.XPATH, "//span[text()='汪文']/../..//input").click()
#         self.driver.find_element(By.XPATH, "//*[text()='删除']").click()
#         self.driver.find_element(By.XPATH, "//*[@d_ck='submit_hr_helper']").click()
#         loc_tips = (By.ID, "js_tips")
#         # 等到可见，再去获取结果文字
#         WebDriverWait(self.driver, 10, 2).until(expected_conditions.visibility_of_element_located(loc_tips))
#         print(f"冒泡消息：{loc_tips}")
