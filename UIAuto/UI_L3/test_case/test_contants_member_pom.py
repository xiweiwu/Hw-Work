from datetime import time

from faker import Faker

from UIAuto.UI_L3.page_object.login_page import LoginPage


class TestContant:
    def setup_class(self):
        self.wework = LoginPage()
        # 假数据
        fake = Faker('zh_CN')
        self.username = fake.name()
        self.acctid = fake.ssn()
        self.mobile = fake.phone_number()

    def teardown_class(self):
        # pass
        # time.sleep(20)
        self.wework.do_quite()

    def test_add_member(self):
        # 登录企业微信，进入企业微信首页
        result = self.wework \
            .login()\
            .click_add_member_btn() \
            .fill_out_info(self.username,self.acctid,self.mobile) \
            .get_tips()

        assert "保存成功" == result
