import allure
import pytest
import requests

from apiAuto.AL_P1.utils.log_utils import logger

"""
宠物商店宠物单接口查询
"""

@allure.feature("宠物搜索接口")
class TestPetStoreSearch:

    def setup_class(self):
        self.base_url = "https://petstore.swagger.io/v2/pet"
        self.search_url = self.base_url + "/findByStatus"

    # 单接口冒烟测试用例
    # def test_search_pet(self):
    #     #宠物查询参数
    #     pet_status={
    #         "status":"available"
    #     }
    #     #发出查询接口请求
    #     r=requests.get(self.search_url,params=pet_status)
    #     #添加日志打印接口响应
    #     logger.info(r.text)
    #     # 状态断言
    #     assert r.status_code ==200
    #     #业务断言
    #     assert  r.json() !=[]
    #     #数组包含Id字段
    #     assert "id" in r.json()[0]

    @pytest.mark.parametrize(
        "status",
        ["available", "pending", "sold"],
        ids=["available_pets", "pending_pets", "sold_pets"]
    )
    @allure.story("宠物搜索冒烟用例")
    def test_search_pet_by_params(self, status):
        # 宠物查询参数
        pet_status = {
            "status": status
        }
        # 发出查询接口请求
        r = requests.get(self.search_url, params=pet_status)
        # 添加日志打印接口响应
        logger.info(r.text)
        # 状态断言
        assert r.status_code == 200
        # 业务断言
        assert r.json() != []
        # 数组包含Id字段
        assert "id" in r.json()[0]

    @pytest.mark.parametrize(
        "status",
        ["petes", "1234", ""],
        ids=["wrog_value", "number", "none_str"]
    )
    @allure.story("status,传入错误值")
    def test_search_pet_fail(self, status):
        # 宠物查询参数
        pet_status = {
            "status": status
        }
        # 发出查询接口请求
        r = requests.get(self.search_url, params=pet_status)
        # 添加日志打印接口响应
        logger.info(r.text)
        # 状态断言
        assert r.status_code == 200
        # 业务断言,数据内容为空
        assert r.json() == []

    @allure.story("传入非status的参数")
    def test_search_pet_wrong_parm(self):
        # 宠物查询参数
        pet_status = {
            "key": "available"
        }
        # 发出查询接口请求
        r = requests.get(self.search_url, params=pet_status)
        # 添加日志打印接口响应
        logger.info(r.text)
        # 状态断言
        assert r.status_code == 200
        # 业务断言,数据内容为空
        assert r.json() == []
