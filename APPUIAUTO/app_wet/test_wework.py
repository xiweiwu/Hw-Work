
from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from faker import Faker
from selenium.common import NoSuchElementException


class TestWework:

    IMPLICITLY_WAIT=10
    def setup_class(self):
        self.faker=Faker("zh_CN")

    def setup(self):
        #准备资源：被测手机信息
        caps={}
        caps["platformName"]="Android"
        caps["platformVersion"] = "6.0"
        caps["deviceName"]="127.0.0.1:7555"
        caps["appPackage"] ="com.tencent.wework"
        caps["appActivity"] =".launch.LaunchSplashActivity"
        caps["noReset"] = True

        self.driver=webdriver.Remote("http://127.0.0.1:4723/wd/hub",caps)
        self.driver.implicitly_wait(self.IMPLICITLY_WAIT)

    def teardown(self):
        self.driver.quit()

    def test_add_conntact(self):
        '''
        1. 打开【企业微信】应用
        2. 进入【通讯录】页面
        3. 点击【添加成员】
        4. 点击【手动输入添加】
        5. 输入【姓名】【手机号】点击【保存】
                '''
        # 准备测试数据
        name=self.faker.name()
        mobile=self.faker.phone_number()

        self.driver.find_element(AppiumBy.XPATH,"//*[@text='通讯录']").click()
        # self.driver.find_element(AppiumBy.XPATH, "//*[@text='添加成员']").click()
        #滑动查找添加成员
        self.swipe_find("添加成员").click()
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='手动输入添加']").click()
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='姓名　']/../*[@text='必填']").send_keys(name)
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='手机　']/..//*[@text='必填']").send_keys(mobile)
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='保存']").click()

        #验证添加成功的toast
        toast_tips=self.driver.find_element(AppiumBy.XPATH,"//*[@class='android.widget.Toast']").text
        print(toast_tips)
        assert  toast_tips == "添加成功"

    def swipe_find(self,text,max_num=3):
        """
        滑动查找一个文本 Text
        如果没有找到圆度，完成滑动操作
        如果找到了，就返回元素
        """
        self.driver.implicitly_wait(1)
        for num in range(max_num):
            try:
                #find_element()每次调用这个方法的时候，都会激活隐式等待，也就是在隐式等待时长之内，动态的查找元素
                element =self.driver.find_element(AppiumBy.XPATH,f"//*[@text='{text}']")
                #找到元素之后，载设置会全局的隐式等待时长10
                self.driver.implicitly_wait(self.IMPLICITLY_WAIT)
                return element
            except:
                print("未找到元素")
                #滑动从下到上
                size=self.driver.get_window_size()
                width=size.get("width")
                height=size.get("height")

                startx= width/2
                starty=height*0.8

                endx=startx
                endy=height*0.2

                duration=2000
                self.driver.swipe(startx,starty,endx,endy,duration)

            if num == max_num-1:
                #没有找到的情况，在抛出异常之前，把这个隐式等待改回全局等待时长10秒
                self.driver.implicitly_wait(self.IMPLICITLY_WAIT)
                raise NoSuchElementException(f"找了{num}次，为未找到{text}")