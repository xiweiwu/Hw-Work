'''
用于测试appium 是否能正常连接移动设备
'''
from appium import webdriver
desired_caps={}
desired_caps['platformName']='Android'
desired_caps['platformVersion']='6.0'
# desired_caps['deviceName']='emulator-5554'
desired_caps['deviceName']='127.0.0.1:7555'
desired_caps['appPackage']='com.android.settings'
desired_caps['appActivity']='com.android.settings.Settings'
driver=webdriver.Remote('http://localhost:4723/wd/hub',desired_caps)